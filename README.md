# Recipe App API Proxy

NGNIX proxy app for our recipe app API

## USAGE

### ENVIRONMENT variables

* `LISTEN_PORT` - Port to listen on (default:`8000`)
* `APP_HOST` - Hostname of the app to forward request to (default:`app`)
* `APP_PORT` - Port of the app to forward the request to (default:`9000`)